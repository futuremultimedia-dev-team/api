
# DEMOTIC API
*Demotic* allows you to interact with all your switches and sensors using JSON, either interactively through a browser or programmatically from a scripting language. In fact, when you interact with the *Demotic* UI through your browser by clicking on buttons and such, under the hood it's JSON that actually communicates your changes back to the *Demotic* engine (Hint: You can watch this behaviour at any time by enabling '''developer tools''' in your browser and watching the ''network'' tab's traffic when you change something through the UI). This wiki describes how to use this versatile API.

# Format

 <nowiki>http://<username:password@>demotic-ip<:port>/json.htm?api-call</nowiki>

* <username:password@> # the username and password to access *Demotic*, this is optional.
* demotic-ip # the IP-address or hostname of your *Demotic* installation. 
* <:port> # the port number of your *Demotic* installation, this is optional. 

 eg. http://192.168.1.2:8080/json.htm?type=command&param=udevice&idx=$idx&nvalue=0&svalue=79

Hint: You can have a look at the database for the nValue/sValue, or the .cpp code.<br>
## Making output more readable
By default, your browser will display the JSON-output in a unordered mess. To make it more readable you could use a browser plugin that formats the JSON to a more human-readable layout. <br>
For Google Chrome you could use [https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc JSONView (Chrome web store)]. For other browsers a similar extension will probably exist. Look in the extensions/add-on store of your browser.

There are online sites which helps to do the same like https://jsonformatter.org. This site helps to format, tree view and validate JSON.

## Authorization
When using some method other than a browser to connect to *Demotic* it may be necessary to do Authorization differently.
Authorization over HTTP is done by setting the "Authorization" HTTP request header when sending the request to *Demotic*. The value of this header is a base64 encoded string of the username and password. When connecting to *Demotic* using a browser and the URL method above the browser sets the appropriate header. When creating your own app or script this is not always done for you.

* First the username and password are combined into one string "username:password"
* This string is encoded using the RFC2045-MIME version of base64
* The authorization method and a space i.e. "Basic " is then put before the encoded string.

This results in a header in the following format:
<pre>Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==</pre>

How the header is send differs per programming language. Check the documentation of the language of your choice on how to set http request headers.

This method works with both the Basic-Auth and Login page options.

## Authorization with parameters
An other way to set username and password when calling the API is :
 <nowiki>http://demotic-ip<:port>/json.htm?username=MkE=&password=OVM=&api-call</nowiki>
* '''MkE=''' is the base64 encoded username ('2A' in this example)
* '''OVM=''' is the base64 encoded password ('9S' in this example)

## Users
When you use an user and password in this api, the user must have "user" or "admin" rights.<br>
He also must have the right to access the device (see 'SetDevices' in users page).

## Base64 encode
To encode a string using base64 you could use : [https://codebeautify.org/base64-encode codebeautify.org/base64-encode]

## Response
All responses include a status which is either '''OK''' on success or '''ERR''' on failure
<pre>{
   "status" : "OK"
}</pre>

# General usage
## Retrieve status of *Demotic* instance

You can get the status of a *Demotic* instance with:

 /json.htm?type=command&param=getversion
<pre>{
"demoticUpdateURL": "http://your-ip/download.php?channel=beta&type=release&system=linux&machine=armv7l",
"HaveUpdate": false,
"Revision": 9540,
"SystemName": "linux",
"UseUpdate": true,
"build_time": "2018-05-29 14:27:24",
"dzvents_version": "2.4.6",
"hash": "30295913",
"python_version": "3.5.3 (default, Jan 19 2017, 14:11:04) \n[GCC 6.3.0 20170124]",
"status": "OK",
"title": "GetVersion",
"version": "3.9530"
}</pre>

## Retrieve *Demotic* logfile information

You can get the last couple (fixed at max. 100)  of logmessages with:

/json.htm?type=command&param=getlog&lastlogtime=LASTLOGTIME&loglevel=LOGLEVEL

* 
  LOGLEVEL 1 # normal
           2 # status
           4 # error
   268435455 # all

  LASTLOGTIME  starting with logmessages in LASTLOGTIME seconds since last epoch ( 0 # all available)   

(In older versions &loglevel is ignored and the system will return all types) Somewhere around V3.9580 this changed and without the loglevel set, the system will only return type 1 messages

## Retrieve status of specific device

You can get the status of a specific device with:

 /json.htm?type=devices&rid=IDX

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")

## Get all devices of a certain type

 /json.htm?type=devices&filter=all&used=true&order=Name

This will return information about all the devices. If you want to retrieve information for a certain devicetype, you can use the following filters:

 light # Get all lights/switches
 weather # Get all weather devices
 temp # Get all temperature devices
 utility # Get all utility devices

## Get all Favorite devices

 /json.htm?type=devices&used=true&filter=all&favorite=1

This will return only devices that are tagged as Favorite.

### Get sunrise and sunset times

 /json.htm?type=command&param=getSunRiseSet

<pre>{
   "ServerTime" : "Sep 30 2013 17:15:21",
   "Sunrise" : "07:38:00",
   "Sunset" : "19:16:00",
   "status" : "OK",
   "title" : "getSunRiseSet"
}</pre>

## Add a log message to the *Demotic* log

You can add a log message to the system with:

 /json.htm?type=command&param=addlogmessage&message=MESSAGE

* MESSAGE # a string you want to log

## Use Notification Subsystem

You can post a notification to all enabled notification systems with:

 /json.htm?type=command&param=sendnotification&subject=SUBJECT&body=THEBODY

You can post a notification to a specific notification system with:

 /json.htm?type=command&param=sendnotification&subject=SUBJECT&body=THEBODY&subsystem=SUBSYSTEM

* SUBJECT # a string you want to use as subject
* THEBODY# a string you want to use as body
* SUBSYSTEM# the notification method you want to use
 gcm
 http
 kodi
 lms
 nma
 prowl
 pushalot
 pushbullet
 pushover
 pushsafer

## Get settings

 /json.htm?type=settings

This will return the settings defined in "Setup" under a JSON form.
For ex. :
<pre>{
   "AcceptNewHardware" : 0,
   "ActiveTimerPlan" : 0,
   "ClickatellEnabled" : "",
   "ClickatellFrom" : "",
   "ClickatellPassword" : "",
...
}</pre>

# Lights and switches
## Get all lights/switches

 /json.htm?type=command&param=getlightswitches

OR

  /json.htm?type=devices&filter=light&used=true&order=Name

Example output:
<pre>

{
   "result" : [
      {
         "IsDimmer" : false,
         "Name" : "Bedroom lights",
         "SubType" : "Energenie",
         "Type" : "Lighting 1",
         "idx" : "43"
      },
      {
         "IsDimmer" : false,
         "Name" : "Hall",
         "SubType" : "Energenie",
         "Type" : "Lighting 1",
         "idx" : "30"
      },
      {
         "IsDimmer" : true,
         "Name" : "Lounge Light Front",
         "SubType" : "RGBW",
         "Type" : "Lighting Limitless/Applamp",
         "idx" : "32"
      },
      {
         "IsDimmer" : true,
         "Name" : "Lounge Lights All",
         "SubType" : "RGBW",
         "Type" : "Lighting Limitless/Applamp",
         "idx" : "66"
      },
      {
         "IsDimmer" : true,
         "Name" : "Lounge light back",
         "SubType" : "RGBW",
         "Type" : "Lighting Limitless/Applamp",
         "idx" : "33"
      }
   ],
   "status" : "OK",
   "title" : "GetLightSwitches"
}
</pre>

## Turn a light/switch on

 /json.htm?type=command&param=switchlight&idx=99&switchcmd=On

## Turn a light/switch off

 /json.htm?type=command&param=switchlight&idx=99&switchcmd=Off

* idx # id of your device (in this example 99). 
* switchcmd # "On" or "Off" (case sensitive!)


<pre>{
   "status" : "OK",
   "title" : "SwitchLight"
}</pre>

beware that if the switch has the 'protected' attribute set then you need add key `passcode` (with the right passcode)

 /json.htm?type=command&param=switchlight&idx=99&switchcmd=Off&passcode=YOUR_SWITCH_PASSWORD_AS_SET_IN_SETTINGS

or the status will be

<pre>{  
   "message" : "WRONG CODE",
   "status" : "ERROR",
   "title" : "SwitchLight"
}</pre>

## Set a dimmable light to a certain level

 /json.htm?type=command&param=switchlight&idx=99&switchcmd=Set%20Level&level=6

* Some lights have 100 dim levels (like zwave and others), other hardware (kaku/lightwaverf) have other ranges like 16/32
* Level should be the dim level (not percentage), like 0-16 or 0-100 depending on the hardware used
* When the light is off, it will be turned on

<pre>{
   "status" : "OK",
   "title" : "SwitchLight"
}</pre>

## Set a light to a certain color or color temperature

Color can be specified as either hue + brightness, RGB + optional brightness or a *Demotic* JSON color object

;Specify hue and brightness
: Hue in degrees, 0..360
: Brightness 0..100
: <code>/json.htm?type=command&param=setcolbrightnessvalue&idx=99&hue=274&brightness=40&iswhite=false</code>
: Note: There is no saturation parameter, specify RGB format *Demotic* JSON color format for full color control

;Specify RGB and brightness (optional)
: RGB as hexadecimal number, RRGGBB without prefix (no 0x or #)
: <code>/json.htm?type=command&param=setcolbrightnessvalue&idx=99&hex=RRGGBB&brightness=100&iswhite=false</code>
: Note: The actual brightness of the light is a combination of RGB value and brightness. Brightness parameter may be omitted.
: A site like [http://www.colorhexa.com/ff8c00 colorhexa] can be used to help pick hex values according to desired colours.

;Specify *Demotic* JSON color object and brightness
: Example: Specify color mode with a dim blue color
: <code>/json.htm?type=command&param=setcolbrightnessvalue&idx=130&color={"m":3,"t":0,"r":0,"g":0,"b":50,"cw":0,"ww":0}&brightness=100</code>

'''*Demotic* color format:'''

*Demotic* color format is used as:
* input parameter to:
*:- setcolbrightnessvalue
*:- updatetimer
*:- addtimer
*:- addscenedevice
*:- updatescenedevice
* In response from:
*:- devices
*:- getscenedevices
*:- timers

Example: <code>color={"m":3,"t":0,"r":0,"g":0,"b":50,"cw":0,"ww":0}</code>
 <nowiki>
ColorMode {
	ColorModeNone # 0,   // Illegal
	ColorModeWhite # 1,  // White. Valid fields: none
	ColorModeTemp # 2,   // White with color temperature. Valid fields: t
	ColorModeRGB # 3,    // Color. Valid fields: r, g, b.
	ColorModeCustom # 4, // Custom (color + white). Valid fields: r, g, b, cw, ww, depending on device capabilities
	ColorModeLast # ColorModeCustom,
};

Color {
	ColorMode m;
	uint8_t t;     // Range:0..255, Color temperature (warm / cold ratio, 0 is coldest, 255 is warmest)
	uint8_t r;     // Range:0..255, Red level
	uint8_t g;     // Range:0..255, Green level
	uint8_t b;     // Range:0..255, Blue level
	uint8_t cw;    // Range:0..255, Cold white level
	uint8_t ww;    // Range:0..255, Warm white level (also used as level for monochrome white)
}
</nowiki>

## Set an RGB_CW_WW or CW_WW light to a certain color temperature

 /json.htm?type=command&param=setkelvinlevel&idx=99&kelvin=1
 Range of kelvin parameter: 0..100, 0 is coldest, 100 is warmest

## Toggle a switch state between on/off

 /json.htm?type=command&param=switchlight&idx=99&switchcmd=Toggle

<pre>{
   "status" : "OK",
   "title" : "SwitchLight"
}</pre>

# Scenes / Groups
## Get all the scenes & groups

 /json.htm?type=scenes

Example output:
<pre>{
   "result" : [
      {
         "Favorite" : 1,
         "HardwareID" : 0,
         "LastUpdate" : "2013-09-29 19:11:01",
         "Name" : "My Scene",
         "Status" : "Off",
         "Timers" : "true",
         "Type" : "Scene",
         "idx" : "9"
      },
      {
         "Favorite" : 1,
         "HardwareID" : 0,
         "LastUpdate" : "2013-09-30 11:49:13",
         "Name" : "My Group",
         "Status" : "Off",
         "Timers" : "false",
         "Type" : "Group",
         "idx" : "3"
      }
   ],
   "status" : "OK",
   "title" : "Scenes"
}</pre>

# Turn a scene / group on or off

 /json.htm?type=command&param=switchscene&idx=&switchcmd=

* idx # id of your scene/group. 
* switchcmd # "On", "Off" or "Toggle" (case sensitive!)
* Scenes can only be turned '''On'''

<pre>{
   "status" : "OK",
   "title" : "SwitchScene"
}</pre>

### Add a scene (0)
 /json.htm?type=addscene&name=''scenename''&scenetype=0

### Add a group (1)
 /json.htm?type=addscene&name=''scenename''&scenetype=1

### Delete a scene or group
 /json.htm?type=deletescene&idx=''number''

### List devices in a scene
 /json.htm?type=command&param=getscenedevices&idx=''number''&isscene=true

### Add device to a scene
 /json.htm?type=command&param=addscenedevice&idx=''number''&isscene=true&devidx=''deviceindex''&command=1&level=''number''&hue=''number''

### Delete device from a scene
 /json.htm?type=command&param=deletescenedevice&idx=''number''

### List activation devices of a scene
 /json.htm?type=command&param=getsceneactivations&idx=''number''

Example output:
<pre>{
    "result": [
       {
           "code": 0,
           "codestr": "-",
           "idx": 249,
           "name": "Activator device"
       },
       {
            "code": 0,
            "codestr": "-",
            "idx": 1048,
            "name": "Another Activator device"
       }
    ],
    "status": "OK",
    "title": "GetSceneActivations"
}</pre>

### List timers of a scene
 /json.htm?type=scenetimers&idx=''number''

Example output:
<pre>{
   "result" : [
      {
         "Active" : "true",
         "Cmd" : 0,
         "Date" : "07-02-2016",
         "Days" : 128,
         "Hue" : 0,
         "Level" : 100,
         "Randomness" : true,
         "Time" : "00:01",
         "Type" : 5,
         "idx" : "16"
      }
   ],
   "status" : "OK",
   "title" : "SceneTimers"
}</pre>

## Add timer to a scene

 /json.htm?type=command&param=addscenetimer&idx=''number''&active=&timertype=&date=&hour=&min=&randomness=&command=&level=&days=

* idx        # index of your scene/group.
* active     # true/false
* timertype  # 0 # Before Sunrise, 1 # After Sunrise, 2 # On Time, 3 # Before Sunset, 4 # After Sunset, 5 # Fixed Date/Time
* date       # MM-DD-YYYY
* hour       # hour
* min        # minute
* randomness # true/false
* command    # On/Off
* level      # 0..100 (%)
* days       # 0x80 # Everyday, 0x100 # Weekdays, 0x200 # Weekends, 0x01 # Mon, 0x02 # Tue, 0x04 # Wed, 0x08 # Thu, 0x10 # Fri, 0x20 # Sat, 0x40 # Sun

###Enable/disable/delete specific scene schedule(timer)
[[demotic_API/JSON_URL's#Enable specific schedule(timer) for Scenes/Groups|Enable specific schedule(timer) for Scenes/Groups]]

[[demotic_API/JSON_URL's#Disable specific schedule(timer) for Scenes/Groups|Disable specific schedule(timer) for Scenes/Groups]]

[[demotic_API/JSON_URL's#Delete specific schedule(timer) for Scenes/Groups|Delete specific schedule(timer) for Scenes/Groups]]

# Server control

## Shutdown system
 /json.htm?type=command&param=system_shutdown

<pre>{
   "status" : "OK",
   "title" : "SystemShutdown"
}</pre>

###Reboot system
 /json.htm?type=command&param=system_reboot

<pre>{
   "status" : "OK",
   "title" : "SystemReboot"
}</pre>

# Create commands

## Create virtual hardware
 /json.htm?type=command&param=addhardware&htype=15&port=1&name=Sensors1&enabled=true

afterward to get the id, either you have your last created id from an index you maintain or sort the hardware page for last ID:
  /json.htm?type=hardware

## Create a virtual sensor

Temp+Humidity (see below for values)
 /json.htm?type=createvirtualsensor&idx=29&sensorname=TempHum&sensortype=82
Electricity (see below for values)
 /json.htm?type=createvirtualsensor&idx=29&sensorname=Energy&sensortype=90

and then get the device id from the list:
 /json.htm?type=devices&filter=all&used=true&order=Name

 1 Pressure (Bar) 0.0 	      	     nvalue=BAR (TBC)        
 2 Percentage     0.0 	      	     nvalue=PCT (TBC)	        
 80 TEMP          0.0     	     svalue=TEMP
 81 HUM           1		     nvalue=HUM svalue=1 to 3
 82 TEMP_HUM      0.0;50;1 	     svalue=TEMP;HUM;HUM_STATUS
 84 TEMP_HUM_BARO 0.0;50;1;1010;1    svalue=TEMP;HUM;HUM_STATUS;BARO;BARO_FCST
 85 RAIN          0;0		     svalue=RAIN;Rain in mm/h
 86 WIND          0;N;0;0;0;0  	     svalue=WIN_SPD;WIND_DIR;?;?;?;?
 87 UV            0;0		     svalue# (TBC)
 113 RFXMeter     0		     Can have several values, another order has to be sent to set the sub type:	   
 							type 3	Counter:   svalue=COUNTER
 							type 2	Water:	   svalue=VOLUME
 							type 1	Gas:
 							type 0	Energy:
 90 ENERGY        0;0.0		     svalue=POWER;ENERGY
 249 TypeAirQuality    0	     nvalue=PPM

For the RFXMeter, another request is needed to set the utility, url is:
 /json.htm?type=setused&idx=DEVICE_ID&name=RFXMeter&switchtype=SUBTYPE_VALUE&used=true
where DEVICE_ID is the device name, and the SUBTYPE_VALUE is one of:
 0 for Energy
 1 for Gas
 2 for Water
 3 for Counter

# Update devices/sensors
Expert usage. Directly set device parameters via JSON.
<br>
First go to the devices tab and notice the device index (idx) of the device you want to change<br>
<br>

## Temperature

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=TEMP

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* TEMP # Temperature<br>
<br>

## Humidity
 /json.htm?type=command&param=udevice&idx=IDX&nvalue=HUM&svalue=HUM_STAT

The above sets the parameters for a Humidity device<br>
* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* HUM # Humidity: 45%<br>
* HUM_STAT # Humidity_status<br>
<br>
Humidity_status can be one of:<br>
*0=Normal<br>
*1=Comfortable<br>
*2=Dry<br>
*3=Wet<br>
<br>

## Barometer

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=BAR;BAR_FOR

The above sets the parameters for a Barometer device from hardware type 'General'<br>
* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* BAR # Barometric pressure<br>
* BAR_FOR # Barometer forecast<br>
<br>
Barometer forecast can be one of:<br>
* 0 # Stable<br>
* 1 # Sunny<br>
* 2 # Cloudy<br>
* 3 # Unstable<br>
* 4 # Thunderstorm<br>
<br>

## Temperature/humidity

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=TEMP;HUM;HUM_STAT
* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* TEMP # Temperature<br>
* HUM # Humidity (0-100 %)<br>
* HUM_STAT # Humidity status<br>
<br>
HUM_STAT can be one of:<br>
* 0=Normal<br>
* 1=Comfortable<br>
* 2=Dry<br>
* 3=Wet<br>
<br>

## Temperature/humidity/barometer

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=TEMP;HUM;HUM_STAT;BAR;BAR_FOR

The above sets the parameters for a Temp+Humidity+Barometer device<br>
* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* TEMP # Temperature<br>
* HUM # Humidity<br>
* HUM_STAT # Humidity status<br>
* BAR # Barometric pressure<br>
* BAR_FOR # Barometer forecast<br>
<br>
Barometer forecast can be one of:<br>
* 0 # No info<br>
* 1 # Sunny<br>
* 2 # Partly cloudy<br>
* 3 # Cloudy<br>
* 4 # Rain<br>
<br>
## Temperature/barometer

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=TEMP;BAR;BAR_FOR;ALTITUDE

The above sets the parameters for a Temp+Humidity+Barometer device<br>
* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* TEMP # Temperature<br>
* BAR # Barometric pressure<br>
* BAR_FOR # Barometer forecast<br>
* ALTITUDE# Not used at the moment, can be 0<br>
<br>
Barometer forecast can be one of:<br>
0 # No Info<br>
1 # Sunny<br>
2 # Paryly Cloudy<br>
3 # Cloudy<br>
4 # Rain<br>
<br>

## Rain

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=RAINRATE;RAINCOUNTER

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* RAINRATE # amount of rain in last hour in [mm x 100]
* RAINCOUNTER # continues counter of fallen Rain in [mm]

## Soil Moisture

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=MOISTURE

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* MOISTURE # moisture content in cb 0-200 where:
00 - 09 # saturated,
10 - 19 # adequately wet,
20 - 59 # irrigation advice,
60 - 99 # irrigation,
100-200 # Dangerously dry,

### Wind

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=WB;WD;WS;WG;22;24

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* WB # Wind bearing (0-359)<br>
* WD # Wind direction (S, SW, NNW, etc.)<br>
* WS # 10 * Wind speed [m/s]<br>
* WG # 10 * Gust [m/s]<br>
* 22 # Temperature<br>
* 24 # Temperature Windchill<br>

### UV

  /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=UV;TEMP

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* UV # Float (example: 2.1) with current UV reading.<br><br>
* TEMP # temperature (only for UV subtype TFA (3), else specify 0)
Don't loose the ";0" at the end - without it database may corrupt.

If no temp is in use, specify 0 as temperature

### Counter

 /json.htm?type=command&param=udevice&idx=IDX&svalue=COUNTER

* IDX # id of your device (this number can be found in the devices tab in the column "IDX")
* COUNTER # Integer of the overall total volume.<br><br>

When there is a counter created, there is a possibility to change the units by clicking on "Change" at the utility tab.

* Energy (kWh)<br>
* Gas (m<sup>3</sup>)<br>
* Water (m<sup>3</sup>)<br>
* Counter (no unit)<br>

The counter will be treated with the divider which is defined in the parameters in the application settings. For example if the counter is set to "Water" and the value is passed as liters, the divider must set to 1000 (as the unit is m<sup>3</sup>).
The device displays 2 values:<br>
* The status is the overall total volume (or counter).<br>
* The volume (or counter) of the day (in the top right corner).<br>
The today's volume (or counter) is calculated from the total volume (or counter).

## Electricity (instant and counter)

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=POWER;ENERGY

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* POWER # current power<br>
* ENERGY # cumulative energy in Watt-hours (Wh) This is an incrementing counter. (if you choose as type "Energy read : Computed", this is just a "dummy" counter, not updatable because it's the result of *Demotic* calculs from POWER)<br>

## Electricity Current/Ampere 3 Phase

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=Ampere_1;=Ampere_2;=Ampere_3;

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* Ampere_1 # Ampere value Phase 1
* Ampere_2 # Ampere value Phase 2
* Ampere_3 # Ampere value Phase 3

## Electricity P1 smart meter

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=USAGE1;USAGE2;RETURN1;RETURN2;CONS;PROD

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* USAGE1# energy usage meter tariff 1, This is an incrementing counter
* USAGE2# energy usage meter tariff 2, This is an incrementing counter
* RETURN1# energy return meter tariff 1, This is an incrementing counter
* RETURN2# energy return meter tariff 2, This is an incrementing counter
* CONS# actual usage power (Watt)
* PROD# actual return power (Watt)
USAGE and RETURN are counters (they should only count up).<br>
For USAGE and RETURN supply the data in total Wh with no decimal point.<br>
(So if your meter displays f.i. USAGE1# 523,66 KWh you need to send 523660)<br>

## Air quality

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=PPM

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* PPM # CO<sub>2</sub>-concentration

## Pressure

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=BAR
* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* BAR # Pressure in Bar
Create sensor under Hardware > Dummy > Create virtual sensor

## Percentage

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=PERCENTAGE

* IDX # id of your device (this number can be found in the devices tab in the column "IDX")
* PERCENTAGE # Percentage

## Visibility

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=VISIBILITY

* IDX # id of your device (this number can be found in the devices tab in the column "IDX")
* VISIBILITY # in KM

## Gas

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=USAGE

* USAGE# Gas usage in liter (1000 liter # 1 m³)<br>
So if your gas meter shows f.i. 145,332 m³ you should send 145332.<br>
The USAGE is the total usage in liters from start, not f.i. the daily usage.

## Lux

 /json.htm?type=command&param=udevice&idx=IDX&svalue=VALUE

* IDX # device ID of Lux device
* VALUE # value of luminosity in Lux (INTEGER/FLOAT)

## Voltage

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=VOLTAGE

* IDX # device ID of Voltage device
* VALUE # value of voltage sensor in Volts
Create sensor under Hardware > Dummy > Create virtual sensor

## Text sensor

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=TEXT

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* TEXT # Text you want to display

## Alert sensor

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=LEVEL&svalue=TEXT

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* Level # (0=gray, 1=green, 2=yellow, 3=orange, 4=red)
* TEXT # Text you want to display

## Distance sensor

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=DISTANCE

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* DISTANCE # distance in cm or inches, can be in decimals. For example 12.6

## Selector Switch

 /json.htm?type=command&param=switchlight&idx=IDX&switchcmd=Set%20Level&level=LEVEL

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* LEVEL # level of your selector (This number can be found in the edit selectors page, in the column "Level", 0 # Off)

## Custom Sensor

 /json.htm?type=command&param=udevice&idx=IDX&nvalue=0&svalue=VALUE

* IDX # id of your device (This number can be found in the devices tab in the column "IDX")
* VALUE # Value (like 12.345)<br>
<br>

### Additional parameters (signal level & battery level)
There are two additional parameters for the above commands, to specify the '''signal level''' (default 12) and the '''battery level''' (default 255)<br>
<br>
battery level 255 # no battery device, else 0-100<br>
example: '''&rssi=10&battery=89'''

# User variables

### Store a new variable

 json.htm?type=command&param=saveuservariable&vname=uservariablename&vtype=uservariabletype&vvalue=uservariablevalue

Where type is 0 to 4:

 0 # Integer, e.g. -1, 1, 0, 2, 10 
 1 # Float, e.g. -1.1, 1.2, 3.1
 2 # String
 3 # Date in format DD/MM/YYYY
 4 # Time in 24 hr format HH:MM
 5 # DateTime (but the format is not checked)

All formats (except 5) are checked by the api, when the variable does not match the required format it is not stored.

### Update an existing variable

 /json.htm?type=command&param=updateuservariable&vname=USERVARIABLENAME&vtype=USERVARIABLETYPE
 &vvalue=USERVARIABLEVALUE

Remember to change:
* USERVARIABLENAME with the name of your variable
* USERVARIABLETYPE according to the table from [[demotic_API/JSON_URL's#Store_a_new_variable|"Store a new variable"]]
* USERVARIABLEVALUE with the new value

### List all variables

 /json.htm?type=command&param=getuservariables
### List one variable

 /json.htm?type=command&param=getuservariable&idx=IDX
* IDX # id of your variable (This number can be found using the "List all variables" call above)

### Delete a variable

 /json.htm?type=command&param=deleteuservariable&idx=IDX
* IDX # id of your variable (This number can be found using the "List all variables" call above)


# Room Plans

### List all rooms
 /json.htm?type=plans&order=name&used=true

### List all devices in a roo
 /json.htm?type=command&param=getplandevices&idx=IDX
* IDX # id of your room

# History

## Switch
 /json.htm?type=lightlog&idx=IDX

## Temperature
 /json.htm?type=graph&sensor=temp&idx=IDX&range=day
 /json.htm?type=graph&sensor=temp&idx=IDX&range=month
 /json.htm?type=graph&sensor=temp&idx=IDX&range=year

## Setpoint
 See Temperature

## Energy, Gas, Water


Instantaneous consumption :

 /json.htm?type=graph&sensor=counter&idx=IDX&range=day&method=1

Totals by period :
 /json.htm?type=graph&sensor=counter&idx=IDX&range=day  
 /json.htm?type=graph&sensor=counter&idx=IDX&range=month
 /json.htm?type=graph&sensor=counter&idx=IDX&range=year

# Device Timer Schedules
### Get all schedules (timers
 ../json.htm?type=schedules
Typical result :
<pre>
{
   "result" : [
      {
         "Active" : "true",
         "Date" : "",
         "Days" : 128,
         "DevName" : "Porch Light",
         "DeviceRowID" : 52,
         "Hue" : 0,
         "IsThermostat" : "false",
         "Level" : 100,
         "MDay" : 0,
         "Month" : 0,
         "Occurence" : 0,
         "Randomness" : "false",
         "ScheduleDate" : "2016-04-01 20:33:00",
         "Time" : "00:20",
         "TimerCmd" : 0,
         "TimerID" : 9,
         "TimerType" : 4,
         "TimerTypeStr" : "After Sunset",
         "Type" : "Device"
      },
</pre>

### Get all schedules(timers) for Devices
 ../json.htm?type=schedules&filter=device
### Get all schedules(timers) for Scene
 ../json.htm?type=schedules&filter=scene
### Get all schedules(timers) for ThermostatS
 ../json.htm?type=schedules&filter=thermostat
### Enable specific schedule(timer) for DeviceS
<pre>../json.htm?type=command&param=enabletimer&idx=timerID</pre>
### Disable specific schedule(timer) for Devices
<pre>../json.htm?type=command&param=disabletimer&idx=timerID</pre>
### Delete a specific schedule(timer) for Devices
<pre>../json.htm?type=command&param=deletetimer&idx=timerID</pre>
### Update parameters for a specific schedule(timer) for Devices
<pre>../json.htm?type=command&param=updatetimer&idx=timerID&active=true&timertype=3&hour=0&min=20&randomness=true&command=0&days=12345</pre>

* idx        # timerID
* active     # true/false
* timertype  # 0 # Before Sunrise, 1 # After Sunrise, 2 # On Time, 3 # Before Sunset, 4 # After Sunset, 5 # Fixed Date/Time
* hour       # hour
* min        # minute
* randomness # true/false
* command    # On=0 / Off=1 (inverted logic)
* days       # 1=Mon, 2=Tue, 3=Wed, 4=Thurs, 5=Fri, 6=Sat, 7=Sun

### Create a new schedule(timer) for an existing device
<pre>../json.htm?type=command&param=addtimer&idx=DeviceRowID&active=true&timertype=2&hour=0&min=20&randomness=false&command=0&days=1234567</pre>

* idx        # DeviceRowID # index of your device (other commands use a timerID as idx but here we use DeviceRowID).
* active     # true/false
* timertype  # 0 # Before Sunrise, 1 # After Sunrise, 2 # On Time, 3 # Before Sunset, 4 # After Sunset, 5 # Fixed Date/Time
* hour       # hour
* min        # minute
* randomness # true/false
* command    # On=0 / Off=1 (inverted logic)
* days       # 1=Mon, 2=Tue, 3=Wed, 4=Thurs, 5=Fri, 6=Sat, 7=Sun

### Clear all schedules(timers) for a particular device
<pre>../json.htm?type=command&param=cleartimers&idx=DeviceRowID</pre>

* idx        # DeviceRowID # index of your device (other commands use a timerID as idx but here we use DeviceRowID).

### Enable specific schedule(timer) for Scenes/Groups
<pre>../json.htm?type=command&param=enablescenetimer&idx=timerID</pre>
### Disable specific schedule(timer) for Scenes/Groups
<pre>../json.htm?type=command&param=disablescenetimer&idx=timerID</pre>
### Delete a specific schedule(timer) for Scenes/Groups
<pre>../json.htm?type=command&param=deletescenetimer&idx=timerID</pre>

# Configuring zwave
### Listing zwave node parameters
<pre>../json.htm?type=openzwavenodes&idx=controlleridx</pre>

* controlleridx    # the idx of your OpenZWave USB controller device, as listed under the 'Setup --> Hardware' tab.

Example output:

<pre>
{
   "NodesQueried" : true,
   "ownNodeId" : 1,
   "result" : [
      {
         "Description" : "Neo CoolCam Door/Window Detector",
         "HaveUserCodes" : false,
         "HomeID" : 3554707869,
         "IsPlus" : true,
         "LastUpdate" : "2018-08-04 20:09:02",
         "Manufacturer_id" : "0x0258",
         "Manufacturer_name" : "Neo CoolCam",
         "Name" : "Garage DOor",
         "NodeID" : 40,
         "PollEnabled" : "false",
         "Product_id" : "0x1082",
         "Product_name" : "Door/Window Detector",
         "Product_type" : "0x0003",
         "State" : "Sleeping",
         "Version" : 4,
         "config" : [
            {
               "LastUpdate" : "2018-08-01 09:06:45",
               "help" : "This configuration parameter that can be used to adjust the amount of delay before the OFF command is transmitted. This parameter can be configured with the value of 0 through 65535, where 0 means send OFF command immediately and 65535 means 65535 seconds of delay.",
               "index" : 1,
               "label" : "Configuring the OFF Delay",
               "type" : "short",
               "units" : "second",
               "value" : "0"
            },
            {
               "LastUpdate" : "2018-08-01 09:06:45",
               "help" : "Basic Set Command will be sent where contains a value when the door/window is opened or closed, the receiver will take it for consideration; for instance, if a lamp module is received the Basic Set Command of which value is decisive as to how bright of dim level of lamp module shall be.",
               "index" : 2,
               "label" : "Basic Set Level",
               "type" : "byte",
               "units" : "",
               "value" : "255"
            },
            {
               "LastUpdate" : "2018-08-03 07:55:52",
               "help" : "",
               "index" : 2000,
               "label" : "Wake-up Interval",
               "type" : "int",
               "units" : "Seconds",
               "value" : "43200"
            }
         ],
         "idx" : "11"
      },
     {
         "Description" : "FIBARO System FGWPE/F Wall Plug",
         "HaveUserCodes" : false,
         "HomeID" : 3554707869,
         "IsPlus" : false,
         "LastUpdate" : "2018-08-05 12:12:24",
         "Manufacturer_id" : "0x010f",
         "Manufacturer_name" : "FIBARO System",
         "Name" : "Studio",
         "NodeID" : 5,
         .
         .
         .

</pre>

Note the ''node_idx'' towards the end of each device's entry (the "idx" : "11" in the example above for the Neo CoolCam Door/Window Detector). You don't see this node_idx in the regular devices tab for *Demotic*, but it is the idx you'll need when changing a parameter through JSON (next section).

### Changing a zwave device parameter
<pre>../json.htm?type=command&param=applyzwavenodeconfig&idx=node_idx&valuelist=plist</pre>

* node_idx #    the value found by searching through the output of ''openzwavenodes'' for the node in question (see above)
* plist    #    the (partially) URI encoded and BASE64 encoded parameter list

The ''plist'' entries start with an unencoded decimal item number preceded by and followed by an underscore (e.g. _7_), followed by the correctly-encoded string. You can check out the encodings at the following links: [https://www.base64decode.org base64decode] and at [https://www.urlencoder.org urlencode]. In Base64, 'Enable' is written as 'RW5hYmxl', so it looks odd but it's easy once you grasp it.

For example, if you are configuring a NEO Coolcam Siren through JSON,

<pre>../json.htm?type=command&param=applyzwavenodeconfig&idx=10&valuelist=7_QWxhcm0gbXVzaWM%3D</pre>

would change parameter 7 (Configure Default Siren On Mode) to 'Alarm music', while

<pre>../json.htm?type=command&param=applyzwavenodeconfig&idx=10&valuelist=7_RG9vciBiZWxsIG11c2lj%3D</pre>

would change it to 'Door bell music' (because 'RG9vciBiZWxsIG11c2lj' is the result of Base64 encoding 'Door bell music'). Note: Any '%3D' and such in the ''plist'' are the result of URI encoding an '=' or other non-alphanumeric character.

The easiest way to learn about this is to change a zwave device parameter manually using your browser while you have '''developer tools''' enabled (hit 'ctrl-shift-i' in Chrome). Look under the ''network'' tab at the JSON sent back by the browser to demotic when you click on 'Apply Configuration for This Device'.

<pre>../json.htm?type=command&param=applyzwavenodeconfig&idx=10&valuelist=3_MjU1_1_SGlnaA%3D%3D_
2_MSBtaW51dGU%3D_4_TG93_5_MTA%3D_6_MQ%3D%3D_7_QWxhcm0gbXVzaWM%3D_8_RW5hYmxl_9_RGlzYWJsZQ%3D%3D_</pre>

The browser will actually send the full parameter list to *Demotic*, including the unchanged values of any parameters you didn't alter. If you modify one of them, however, you'll see that edit reflected in the Base64-encoded JSON string.

[[File:Screenshotjsondebug.png]]

=Language-specific JSON examples=
The following tiny code snippets demonstrate the syntax for using JSON with *Demotic* from some popular programming languages.

## Bash
<pre>
#!/bin/bash
curl -s 'http://192.168.178.12:8080/json.htm?type=devices&rid=104' | jq .result[0].Temp | sed 's/\"//g' | awk '{ print $1 }'

</pre>

### PHP
<pre>
#!/usr/bin/php

 <?php
 $json_string # file_get_contents("http://192.168.178.12:8080/json.htm?type=devices&rid=110");
 $parsed_json # json_decode($json_string, true);
 $test_link # "/home/pi/output.txt";
 $test_data # fopen ($test_link, "w+");
 fwrite ($test_data, print_R($parsed_json, TRUE));
 fclose ($test_data);
 ?>

</pre>
## Python
<pre>
#!/usr/bin/python

import urllib
import urllib2
import requests

...

url # 'http://192.168.178.12:8080/json.htm?type=command&param=udevice&nvalue=0&idx='+idx+'&svalue='+sval
try:
  request # urllib2.Request(url)
  response # urllib2.urlopen(request)
  except urllib2.HTTPError, e:
    print e.code; time.sleep(60)
  except urllib2.URLError, e:
    print e.args; time.sleep(60)
</pre>

## Perl
<pre>
#!/usr/bin/perl

use LWP::UserAgent;
use JSON::XS;

$url{domo}=>'http://192.168.178.12:8080';
$idx=301;

$ua=LWP::UserAgent->new; $ua->timeout(5);
$retrieve=$ua->get($url{domo}.'/json.htm?type=devices&rid='.$idx);
$res=$retrieve->decoded_content;
if ($retrieve->is_success) { $jres # JSON::XS->new->allow_nonref->decode($res)  } else { warn $retrieve->status_line };
$state=$$jres{result}[0]->{Status}; # 'On', 'Off', 'Open', 'Closed', etc
print "The switch state for idx $idx is currently '$state'\n";

</pre>